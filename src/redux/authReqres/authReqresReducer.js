import { actionTypes } from "../action";

const globalState = {
    isLogin: false,
    isLoading: false,
    user: {},
  };

const authReqresReducer = (state = globalState, action) => {
    if(action.type === actionTypes.LOGIN) {
        return{
            ...state,
            isLogin: true,
        };
    }
    if(action.type === actionTypes.CHANGE_LOADING) {
        return{
            ...state,
            isLoading: action.value,
        };
    }  
    if(action.type === actionTypes.CHANGE_USER) {
        return{
            ...state,
            user: action.value,
        };
    }        
    return state;
  };
  

export default authReqresReducer;