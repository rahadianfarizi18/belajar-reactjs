import { actionTypes } from "../action";

const globalState = {
    arrBus: [],
  };

const busReducer = (state = globalState, action) => {
    if(action.type === actionTypes.CHANGE_ARR_BUS) {
        return{
            ...state,
            arrBus: action.value,
        };
    }
    return state;
  };

export default busReducer;