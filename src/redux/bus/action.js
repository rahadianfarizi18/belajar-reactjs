import firebase from '../../config/firebase'
import { database } from '../../config/firebase'

//action types
export const actionTypes = {
    CHANGE_ARR_BUS: "CHANGE_ARR_BUS",
}

export const writeBusAPI = (data) => (dispatch) => {
    return new Promise((resolve, reject) => {
        // const newBusKey = database().ref().child('bus').push().key;
        // let updates = {}
        database.ref('bus/' + data.uid).set({
          arrBus: data.arrBus,
        })
        .then(() => {
            console.log('write bus success')
            resolve(true)
        })
        .catch((error) => {
            var errorCode = error.code;
            var errorMessage = error.message;
            console.log(errorCode, errorMessage)
            reject(false)
        })
    })
  }

export const readBusAPI = () => (dispatch) => {
    const userId = firebase.auth().currentUser.uid;
    return new Promise((resolve) => {
        database.ref('/bus/' + userId).once('value').then((snapshot) => {
        //   const email = (snapshot.val() && snapshot.val().email);
          const arrBus = (snapshot.val() && snapshot.val().arrBus);
          resolve(arrBus)
        })
    })
}