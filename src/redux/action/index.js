import firebase from '../../config/firebase'
import { database } from '../../config/firebase'

//action types
export const actionTypes = {
    PLUS_ORDER: "PLUS_ORDER",
    MINUS_ORDER: "MINUS_ORDER",
    MINUS_PRICE_500: "MINUS_PRICE_500",
    PLUS_PRICE_500: "PLUS_PRICE_500",
    LOGIN: "LOGIN",
    LOGOUT: "LOGOUT",
    CHANGE_NAME: "CHANGE_NAME",
    CHANGE_ARR_BUS: "CHANGE_ARR_BUS",
    CHANGE_LOADING: "CHANGE_LOADING",
    CHANGE_IS_REGISTER: "CHANGE_IS_REGISTER",
    CHANGE_IS_LOGIN_FAILED: "CHANGE_IS_LOGIN_FAILED",
    CHANGE_USER: "CHANGE_USER",
}

export const actionChangeUser = (namaGanti) => (dispatch) => {
    setTimeout(() => {
        return dispatch({type: actionTypes.CHANGE_NAME, payload: namaGanti})
    }, 2000)
}

export const registerUserAPI = (data) => (dispatch) => {
    return new Promise((resolve, reject) => {
        dispatch({type: actionTypes.CHANGE_LOADING, value: true})
        firebase.auth().createUserWithEmailAndPassword(data.email, data.password)
        .then((userCredential) => {
            // Signed in 
            const dataUser = {
                email: userCredential.user.email,
                uid: userCredential.user.uid,
            }
            console.log(userCredential);
            dispatch({type: actionTypes.CHANGE_LOADING, value: false})
            resolve(dataUser)
            // ...
        })
        .catch((error) => {
            var errorCode = error.code;
            var errorMessage = error.message;
            console.log(errorCode, errorMessage)
            dispatch({type: actionTypes.CHANGE_LOADING, value: false})
            reject(false)
            // ..
        })

    })
}

export const loginUserAPI = (data) => (dispatch) => {
    return new Promise((resolve, reject) => {
        dispatch({type: actionTypes.CHANGE_LOADING, value: true})
        firebase.auth().signInWithEmailAndPassword(data.email, data.password)
        .then((userCredential) => {
            // Signed in
            console.log('success login=>',userCredential);
            const dataUser = {
                email: userCredential.user.email,
                uid: userCredential.user.uid,
            }
            dispatch({type: actionTypes.CHANGE_LOADING, value: false})
            dispatch({type: actionTypes.LOGIN})
            resolve(dataUser)
            // ...
        })
        .catch((error) => {
            var errorCode = error.code;
            var errorMessage = error.message;
            console.log(errorCode, errorMessage)
            dispatch({type: actionTypes.CHANGE_LOADING, value: false})
            dispatch({type: actionTypes.CHANGE_IS_LOGIN_FAILED, value: true})
            reject(false)
        })
    })
}

export const writeUserData = (data) => (dispatch) => {
    return new Promise((resolve) => {
        database.ref('users/' + data.uid).set({
          email: data.email,
          name: data.name,
        })
        .then(() => {
            resolve(true)
        })
    })
  }

export const readUserData = () => (dispatch) => {
    const userId = firebase.auth().currentUser.uid;
    return new Promise((resolve) => {
        database.ref('/users/' + userId).once('value').then((snapshot) => {
        //   const email = (snapshot.val() && snapshot.val().email);
          const name = (snapshot.val() && snapshot.val().name);
          resolve(name)
          // ...
        })

    })

}

export const writeBusData = (data) => (dispatch) => {
    return new Promise((resolve) => {
        database.ref('bus/' + data.uid).push({
          arrBus: data.arrBus,
        })
        .then(() => {
            resolve(true)
        })
    })
  }

export const readBusData = () => (dispatch) => {
    //function to read bus data with push to firebase
}