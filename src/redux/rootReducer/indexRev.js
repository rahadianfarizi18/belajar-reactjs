import { combineReducers } from "redux";
import storage from "redux-persist/lib/storage";
import authReducer from "../auth/authReducer";
import busReducer from "../bus/busReducer";
import orderReducer from "../order/orderReducer";

const appReducer = combineReducers({
    auth: authReducer,
    bus: busReducer,
    order: orderReducer,
})

const rootReducer = (state, action) => {
    if(action.type === 'LOGOUT') {
        storage.removeItem('persist:root')
        return appReducer(undefined, action)
    }
    return appReducer(state,action)
}

export default rootReducer;