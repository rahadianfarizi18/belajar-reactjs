import { actionTypes } from "../action";

const globalState = {
    order: 0,
    price: 100000,
    isLogin: false,
    arrBus: [],
    isLoading: false,
    user: {},
    nama: '',
  };

const rootReducer = (state = globalState, action) => {
    if(action.type === actionTypes.PLUS_ORDER) {
        return{
            ...state,
            order: state.order + 1,
        };
    }
    if(action.type === actionTypes.MINUS_ORDER) {
        return{
            ...state,
            order: state.order - 1,
        };
    }
    if(action.type === actionTypes.MINUS_PRICE_500) {
        return{
            ...state,
            price: state.price - 500,
        };
    }
    if(action.type === actionTypes.PLUS_PRICE_500) {
        return{
            ...state,
            price: state.price + 500,
        };
    }
    if(action.type === actionTypes.LOGIN) {
        return{
            ...state,
            isLogin: true,
        };
    }
    if(action.type === actionTypes.LOGOUT) {
        return{
            globalState,
        };
    }  
    if(action.type === actionTypes.CHANGE_NAME) {
        return{
            ...state,
            nama: action.payload,
        };
    }  
    if(action.type === actionTypes.CHANGE_ARR_BUS) {
        return{
            ...state,
            arrBus: action.arrBus,
        };
    }
    if(action.type === actionTypes.CHANGE_LOADING) {
        return{
            ...state,
            isLoading: action.value,
        };
    }  
    if(action.type === actionTypes.CHANGE_USER) {
        return{
            ...state,
            user: action.value,
        };
    }        
    return state;
  };

export default rootReducer;