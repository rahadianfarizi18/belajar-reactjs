import { createStore, applyMiddleware } from "redux";
import thunk from 'redux-thunk'
// import rootReducer from "../rootReducer";
import rootReducer from "../rootReducer/indexRev";
import { persistStore, persistReducer } from 'redux-persist'
import storage from 'redux-persist/lib/storage' // defaults to localStorage for web
import { composeWithDevTools } from "redux-devtools-extension";

const persistConfig = {
    key: 'root',
    storage,
  }

const persistedReducer = persistReducer(persistConfig, rootReducer)


export const globalStore = createStore(
  persistedReducer,
  composeWithDevTools(applyMiddleware(thunk))
  )
export const persistor = persistStore(globalStore)