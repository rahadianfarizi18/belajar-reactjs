import { actionTypes } from "../action";

const globalState = {
    order: 0,
    price: 100000,
    nama:'',
  };

const orderReducer = (state = globalState, action) => {
    if(action.type === actionTypes.PLUS_ORDER) {
        return{
            ...state,
            order: state.order + 1,
        };
    }
    if(action.type === actionTypes.MINUS_ORDER) {
        return{
            ...state,
            order: state.order - 1,
        };
    }
    if(action.type === actionTypes.MINUS_PRICE_500) {
        return{
            ...state,
            price: state.price - 500,
        };
    }
    if(action.type === actionTypes.PLUS_PRICE_500) {
        return{
            ...state,
            price: state.price + 500,
        };
    }
    return state;
  };

export default orderReducer;