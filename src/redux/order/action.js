//action types
export const actionTypes = {
    PLUS_ORDER: "PLUS_ORDER",
    MINUS_ORDER: "MINUS_ORDER",
    MINUS_PRICE_500: "MINUS_PRICE_500",
    PLUS_PRICE_500: "PLUS_PRICE_500",
}