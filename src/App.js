// import logo from './logo.svg';
import './App.css';
// import './style.css';
import { Component } from 'react';
import Home from './component/Home'
import Profile from './component/Profile';
// import Timer from './component/Timer';
import Navbar from './component/Navbar';
import BusManagement from './component/BusManagement';
// import NewBus from './component/BusManagement/NewBus';
import Hooks from './component/Hooks';
import Pokemon from './component/Pokemon';
import {BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import Contributor from './component/Contributor';
import Buku from './component/Buku';
// import { Counter } from './features/counter/Counter';
import Counter from './component/Counter'
import Login from './component/Login'
import LoginReqres from './component/LoginReqres'
import Register from './component/Register';
import Logout from './component/Logout';
import PrivateRoute from './component/PrivateRoute';
import '../node_modules/bootstrap/dist/css/bootstrap.min.css';
import { Container } from 'react-bootstrap';
import ScoreBoard from './component/ScoreBoard';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: 'Rahadian'
    }
  }

  componentDidMount() {
    document.title='RF Site'
  }

  render()  {
    return (
      <Router>
          
            <header>
                <Navbar />
            </header>
            <body className='body'>
            <Container maxWidth='sm'>
                {/* <Counter/> */}
                <Switch>
                <Route path='/' exact component={Home}></Route>
                  {/* <Route path='/' exact component={Home}></Route> */}
                  <Route path='/login' exact component={Login} />
                  <Route path='/register' exact component={Register} />
                  <Route path='/logout' exact component={Logout} />
                  <Route path='/profile' exact component={Profile}></Route>
                  <Route path='/reqres' exact component={LoginReqres} />
                  <PrivateRoute path='/bus-management' exact children={<BusManagement/>} />
                  {/* <Route path='/new-bus' exact component={NewBus} /> */}
                  <PrivateRoute path='/hooks' exact children={<Hooks/>} />
                  <PrivateRoute path='/contributor' exact children={<Contributor />} /> 
                  <PrivateRoute path='/pokemon' exact children={<Pokemon/>} />
                  <PrivateRoute path='/buku' exact children={<Buku/>} />
                  <PrivateRoute path='/counter' exact children={<Counter/>} />
                  <PrivateRoute path='/scoreboard' exact children={<ScoreBoard/>} />
                </Switch>
                {/* <Timer /> */}
              </Container>
            </body>
          
      </Router>
    )
  }
}

export default App;