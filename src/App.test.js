import { render, screen } from '@testing-library/react';
import App from './App';
import { Provider } from 'react-redux'
import { globalStore, persistor } from './redux';
import { PersistGate } from 'redux-persist/integration/react'

test('renders learn react link', () => {
  render(
    <Provider store={globalStore}>
      <PersistGate loading={null} persistor={persistor}>
        <App />
      </PersistGate>
    </Provider>
  );
  const linkElement = screen.getByText(/learn react/i);
  expect(linkElement).toBeInTheDocument();
});
