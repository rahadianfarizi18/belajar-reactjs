import { Link, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import { Container } from 'react-bootstrap';
import '../assets/styles/home.scss'

const Home2 = (props) => {

    if(!props.isLogin) {
        return (
            <Redirect to='/login'/>
        )
    }
    return(
        <Container className='pt-3'>
            <div className='home2' style={{'margin-top':'100px'}}>
                <body className="h-100 text-center text-dark" >
                    <h1>Welcome to RF Site, {props.user.nama}</h1>
                    <p className='lead'>
                        Web ini dibuat dalam rangka belajar React Js pada ajang Telkom Athon #2 2021. <br/>
                        Klik tombol di bawah ini untuk melihat profile saya.
                    </p>
                    <Link to='/profile'>
                        <button className="btn btn-lg btn-secondary fw-bold border-white bg-dark">Learn more</button>
                    </Link>
                </body>
            </div>
        </Container>
    )
}

// const stateToProps = (globalState) => ({
//     isLogin: globalState.isLogin,
//     user: globalState.user,
// })

const stateToProps = (globalState) => ({
    isLogin: globalState.auth.isLogin,
    user: globalState.auth.user,
})

export default connect(stateToProps, null)(Home2);