import Axios from 'axios'
import { Button, Form } from 'react-bootstrap'
import { useState } from 'react'

const Reqres = () => {
    const [ name, setName ] = useState('')
    const [ age, setAge ] = useState(0)
    const [ password, setPassword ] = useState('')
    const [ email, setEmail ] = useState('')

    const handleChangeName = (e) => {
        e.preventDefault();
        setName(e.target.value)
    }

    const handleChangeAge = (e) => {
        e.preventDefault();
        setAge(e.target.value)
    }

    const handleChangePassword = (e) => {
        e.preventDefault();
        setPassword(e.target.value)
    }

    const handleChangeEmail = (e) => {
        e.preventDefault();
        setEmail(e.target.value)
    }

    const handleSubmit = async(e) => {
        e.preventDefault();
        let user = { email, password }
        console.log(user)
        const res = await Axios.post('https://reqres.in/api/login', user).catch(err=>err)
        console.log(res)
    } 

    return(
        <Form>
            <Form.Control type='text' placeholder='Name' value={name} onChange={(e) => handleChangeName(e)}></Form.Control>
            <Form.Control type='text' placeholder='Email' value={email} onChange={(e) => handleChangeEmail(e)}></Form.Control>
            <Form.Control type='number' placeholder='Age' onChange={(e) => handleChangeAge(e)}></Form.Control>
            <Form.Control type='pasword' placeholder='Password' onChange={(e) => handleChangePassword(e)}></Form.Control>
            <Button onClick={(e) => handleSubmit(e)}>Submit</Button>
        </Form>
    )
}

export default Reqres;