import { Route, Redirect } from 'react-router-dom'
import { connect } from 'react-redux'

const PrivateRoute = ({isLogin, children, ...rest}) => {
    return (
        <Route
            {...rest}
            render={({ location }) =>
                isLogin ? (
                children
                ) : (
                <Redirect
                    to={{
                    pathname: "/login",
                    state: { from: location }
                    }}
                />
                )
            }
        />
    );
}


// const stateToProps = (globalState) => {
//     return {
//         isLogin: globalState.isLogin,
//     };
// };

const stateToProps = (globalState) => {
    return {
        isLogin: globalState.auth.isLogin,
    };
};

export default connect(stateToProps, null)(PrivateRoute);