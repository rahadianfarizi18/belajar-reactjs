import Axios from "axios";
import { useState, useEffect } from "react";
import { BrowserRouter as Router, Link } from "react-router-dom";
import { Spinner, Form } from "react-bootstrap";
import '../assets/styles/pokemon.scss'

const Pokemon = (props) => {
    const [urlPokemon, setUrlPokemon] = useState('https://pokeapi.co/api/v2/pokemon/');
    const [nextUrl, setNextUrl] = useState('')
    const [prevUrl, setPrevUrl] = useState('')
    // const [order, setOrder] = useState(1)
    const [pokemonList, setPokemonList] = useState([]);
    const [isLoading, setIsLoading] = useState(true);
    const [numAll, setNumAll] = useState(1);
    const [offSet, setOffSet] = useState(20);
    const [limit, setLimit] = useState(20);
    const [totalPokemon, setTotalPokemon] = useState(0);
    

    useEffect(()=>{
        const fetchData = async() => {
            console.log('limit =>', limit)
            // setUrlPokemon(`https://pokeapi.co/api/v2/pokemon/?offset=${offSet}&limit=${limit}`)
            const response = await(Axios.get(urlPokemon))
            const result =  response.data.results
            setTotalPokemon(response.data.count)
            setNextUrl(response.data.next)
            setPrevUrl(response.data.previous)
            // const responseDetail = await(Axios.get(detailURL))
            // let objPokemons = {}
            let allPokemons = []
            console.log(result)
            let num = numAll;
            for(let data of Object.values(result)) {
                const name = data.name
                const detailURL = data.url
                const responseDetail = await(Axios.get(detailURL))
                const allStats = responseDetail.data.stats
                const objPokemon = {name, num, allStats}
                allPokemons.push(objPokemon)
                num++
            }
            // console.log(responseDetail)
            // console.log(result.data.results)
            // console.log(allPokemons)
            setPokemonList(allPokemons)
            setIsLoading(false)
            
        }
        setIsLoading(true)
        fetchData()
    
    },[urlPokemon, offSet, limit, numAll])

    
    const toTitleCase = str => {
        return(str.replace(/\w\S*/g,(txt) => txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase() ))
    }

    const nextList = () => {
        console.log(nextUrl)
        setUrlPokemon(nextUrl)
        setNumAll(numAll+limit)
        // return( <Redirect to='/pokemon' /> )
    }

    const prevList = () => {
        if(prevUrl) {
            setUrlPokemon(prevUrl)
            console.log(urlPokemon)
            setNumAll(numAll-limit)
        }
    }

    const handleToFirst = () => {
        setUrlPokemon(`https://pokeapi.co/api/v2/pokemon/?offset=${0}&limit=${limit}`)
        setOffSet(0)
        setNumAll(1)
    }

    const handleChangeLimit = (e) => {
        console.log('limit select =>',e.target.value)
        setUrlPokemon(`https://pokeapi.co/api/v2/pokemon/?offset=${0}&limit=${e.target.value}`)
        setLimit(Number(e.target.value))
        setOffSet(0)
        setNumAll(1)
    }

    const printPokemon = () => (
        pokemonList.map((pokemon, i) => (
            <tr>
                <td>{pokemon.num}</td>
                <td>{toTitleCase(pokemon.name)}</td>
                {pokemon.allStats.map((stat,i) => (
                    <td>{stat.base_stat}</td>
                ))}
            </tr>
            )
        )
    )

    return (
        <Router>
            <h1>Pokemon</h1>
            <div className='table-responsive-sm'>
                <table className='table table-hover'>
                    <thead>
                        <tr>
                            <th scope='col'>No.</th>
                            <th scope='col'>Name</th>
                            <th scope='col'>HP</th>
                            <th scope='col'>Attack</th>
                            <th scope='col'>Defense</th>
                            <th scope='col'>Special Attack</th>
                            <th scope='col'>Special Defense</th>
                            <th scope='col'>Speed</th>
                        </tr>
                    </thead>
                    <tbody>
                        {isLoading ? 
                            <Spinner className='spinner-border m-5 text-center' variant='primary'>
                                <span className='visually-hidden'></span>
                            </Spinner> 
                            :
                            printPokemon()}
                        {/* {pokemonList.map((pokemon, i) => (
                            <tr>
                                <td>{toTitleCase(pokemon.name)}</td>
                                {pokemon.allStats.map((stat,i) => (
                                    <td>{stat.base_stat}</td>
                                ))}
                            </tr>
                        ))} */}
                    </tbody>
                </table>
            </div>
            <div>
                <div className='d-flex justify-content-between'>
                    <div md={4}>
                        <nav className='ms-auto' aria-label="Page navigation example">
                            <ul class="pagination">
                                <li class="page-item"><Link className="page-link" onClick={handleToFirst}>First</Link></li>
                                <li class="page-item"><Link className="page-link" onClick={prevList}>Previous</Link></li>
                                <li class="page-item"><Link className="page-link" onClick={nextList}>Next</Link></li>
                            </ul>
                        </nav>
                    </div>
                    <div md={{span:4, offset:4}} className='show-limit'>
                        <Form className='row'>
                            <Form.Label className='col mt-auto mb-auto'>
                                Display
                            </Form.Label>
                            <Form.Select className='col' size='sm' value={limit} onChange={e => handleChangeLimit(e)}>
                                <option value='20'>20</option>
                                <option value='50'>50</option>
                                <option value='100'>100</option>
                                <option value={totalPokemon}>All</option>
                            </Form.Select>
                        </Form>
                    </div>
                </div>
            </div>
        </Router>
    )
}

export default Pokemon;