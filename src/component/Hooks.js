import { useState } from 'react';

const Hooks = () => {
    const [isNama, setIsNama] = useState(false);
    const nama = ['Rahadian','Yeye','Aaron','Lola','Dyah']
    const color = ['text-black', 'text-yellow', 'text-blue', 'text-red', 'text-green']
    const [idx, setIdx] = useState(0)

    const ubahNama1 = () => {
        setIsNama(!isNama);
    }

    const ubahNama2 = () => {
        if(idx < nama.length-1) {
            setIdx(idx+1)
            console.log(idx)
        } else {
            setIdx(0)
        }
    }

    return (
        <>
            <p className={isNama ? 'text-blue' : 'text-red'}>Nama saya: {isNama ? 'Jennie' : 'Rose'}</p>
            <button onClick={ubahNama1}>Ubah nama</button>

            <p className={color[idx]}>Nama saya: {nama[idx]}</p>
            <button onClick={ubahNama2}>Ubah nama</button>
        </>
    )
}

export default Hooks;
