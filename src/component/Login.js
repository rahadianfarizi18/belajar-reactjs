import { useState, useEffect } from "react";
import { Form, Button, Alert } from "react-bootstrap";
import { Link, useHistory, Redirect } from "react-router-dom";
import { connect } from "react-redux";
// import { actionTypes, loginUserAPI, readUserData } from "../redux/action";
import { actionTypes, loginUserAPI, readUserData } from "../redux/auth/action";
import { useLocation } from "react-router";
import logo from '../assets/logo/logo-RF.png'
import "../assets/styles/login.scss"
import Particles from "particlesjs"
import { readBusAPI } from "../redux/bus/action";

const Login = (props) => {
    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')
    const [isLoginFailed, setIsLoginFailed] = useState(null)
    const [ errorCode, setErrorCode ] = useState('')
    let location = useLocation()
    let history = useHistory()
    let { from } = location.state || {from: {pathname: '/'}}

    const handleEmailChange = (e) => {
        e.preventDefault()
        setEmail(e.target.value)
    }

    const handlePasswordChange = (e) => {
        e.preventDefault()
        setPassword(e.target.value)
    }

    const handleSubmit = async (e) => {
        e.preventDefault()
        const res = await props.loginFunction({email, password}).catch(err=>err)
        console.log(res)
        if(res.success) {
            setEmail('')
            setPassword('')
            console.log('res =>', res.value)
            const nama = await props.readUserDataFunction().catch(err=>err)
            console.log('nama =>',nama)
            let arrBus = await props.readBusDataFunction().catch(err=>err)
            if(arrBus===null) arrBus=[]
            console.log('arrBus =>',arrBus)
            props.setUserFunction({
                ...res.value,
                nama
            })
            props.setArrBusFunction(arrBus)
            Particles.pauseAnimation()
            history.push(from)
        } else {
            setIsLoginFailed(true)
            setErrorCode(res.value)
        }

    }

    const alertFrom = () => {
        if(from.pathname !== '/') {
            return (
                <Alert variant="danger">
                    {`You must login to view the page at ${from.pathname}`}
                </Alert>
            )
        }
    }

    const alertLogin = () => {
        if(isLoginFailed) {
            return (
                <Alert variant="danger">
                    Login failed! {errorCode}
                </Alert>
            )
        }
    }

    const loadParticles = () => {
        Particles.init ({ 
            selector: '.background',
            color: '#8790a3',
            connectParticles: true, 
            speed: 0.4,
        });
    }

    useEffect(() => {
        loadParticles()
    }, [])

    if (props.isLogin) {
        return (
            <Redirect to='/'/>
        )
        // return (
        //     history.push('/')
        // )
    } else {
        return(
            <>
                {alertFrom()}
                {alertLogin()}
                {window.onload=() => loadParticles()}
                <div className='form-signin text-center'>
                    {/* <div className='d-flex'> */}
                        <Form type='submit' className='shadow p-4 rounded bg-body'>
                            <img class="mb-2 mx-auto" src={logo} alt="" width="72" height="72" />
                            <h1 className='h3 mt-2 fw-normal'>Login</h1>
                            <Form.Floating>
                                <Form.Control type='email' className='formText' id='email' placeholder='Email' value={email} onChange={(e) => handleEmailChange(e)}/>
                                <Form.Label htmlFor='email'>Email</Form.Label>
                            </Form.Floating>
                            <Form.Floating>
                                <Form.Control type='password' id='password' placeholder='Password' className='mt-2' value={password} onChange={(e) => handlePasswordChange(e)}/>
                                <Form.Label htmlFor='password'>Password</Form.Label>
                            </Form.Floating>
                            <Button color='primary' type='submit' className='w-100 btn-lg' onClick={(e) => handleSubmit(e)}>
                            {props.isLoading ? 
                                (
                                    <>
                                    <span className='spinner-border spinner-border-sm'></span>
                                    {'  '}Loading...
                                    </>
                                ) :
                                "Submit"}
                        </Button>
                            <p className='mt-2'>Don't have account? <Link to='/register'>Register</Link></p>
                        </Form>
                    {/* </div> */}
                </div>
                <canvas className='background'></canvas>
            </>
        )    
    }
}

const stateToProps = (globalState) => ({
        // order: globalState.order,
        // price: globalState.price,
        isLogin: globalState.auth.isLogin,
        isLoading: globalState.auth.isLoading,
        user: globalState.auth.user,
});

const dispatchToProps = (dispatch) => {
    return{
        loginFunction: (data) => dispatch(loginUserAPI(data)),
        setUserFunction: (data) => dispatch({type: actionTypes.CHANGE_USER, value: data}),
        readUserDataFunction: () => dispatch(readUserData()),
        readBusDataFunction: () => dispatch(readBusAPI()),
        setArrBusFunction: (data) => dispatch({type: actionTypes.CHANGE_ARR_BUS, value:data})
    };
};

export default connect(stateToProps, dispatchToProps)(Login);