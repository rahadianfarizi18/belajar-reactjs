import { Component } from 'react';

class Toggle extends Component {
    constructor(props) {
        super(props);
        this.state = {
            toggleStatus: true,
            className: "btn btn-primary"
      }
        this.handleClick = this.handleClick.bind(this);
        this.handleMouseOver = this.handleMouseOver.bind(this);
        this.handleMouseLeave = this.handleMouseLeave.bind(this);
    }
  
    handleClick() {
      this.setState(state => ({
        toggleStatus: !state.toggleStatus
      }))
    }
  
    handleMouseOver() {
      this.setState(() => ({
        className: "btn btn-success"
      }))
    }
  
    handleMouseLeave() {
      this.setState(() => ({
        className: "btn btn-primary"
      }))
    }
  
    render() {
      return(
        <div>
          <button onClick = {this.handleClick} onMouseOver = {this.handleMouseOver} onMouseLeave = {this.handleMouseLeave} className = {this.state.className}>
            Click
          </button>
          <p>{this.state.toggleStatus? "Nyala":"Mati"}</p>
        </div>
      )
    }
}

export default Toggle;