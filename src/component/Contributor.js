import React, { useEffect, useState } from "react";
import Axios from "axios";

const Contributor = (props) => {
    const [users, setUsers] = useState([]);

    const fetchData = async () => {
        const result = await(Axios.get('https://reqres.in/api/users?page=2'))
        setUsers(result.data.data)
    }

    useEffect(()=>{
        fetchData()
    },[])

    console.log(users[0]);

    return(
        <>
            <p className='h3 pt-6'>Big thanks to:</p>
            <div className='row'>
            {users.map((user,i) => (
                <div className='col-auto'>
                    <div class="card" style={{width:'16rem'}} className='col-auto'>
                        <img alt={`${user.first_name} ${user.last_name}`} src={user.avatar} class="card-img-top" />
                        <div class="card-body">
                            <p class="card-text">{`${user.first_name} ${user.last_name}`}</p>
                        </div>
                    </div>
                    {/* <img alt={`${user.first_name} ${user.last_name}`} src={user.avatar}></img>
                    <p>{`${user.first_name} ${user.last_name}`}</p> */}
                </div>
                )
            )}
            </div>
        </>
    )
}

export default Contributor;