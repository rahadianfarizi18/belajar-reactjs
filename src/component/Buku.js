import { useState } from "react";
import imgBuku from '../assets/images/book.jpg';

const Buku = (props) => {
    const [arrBuku,setArrBuku] = useState([])

    const addBook = () => {
        setArrBuku([...arrBuku,(arrBuku.length+1)])
        console.log(arrBuku)
    }
    
    const removeBook = () => {
        let tempArr = [...arrBuku]
        tempArr.pop()
        setArrBuku(tempArr)
        console.log(arrBuku)
    }

    return(
        <>
            <h1>Buku</h1>
            <button onClick={addBook}>Add book</button>
            <button onClick={removeBook}>Remove book</button>
            <ul>
            {arrBuku.map((nilai,idx)=> (
                <li key={idx}><img alt='buku' src={imgBuku} style={{width:'100px'}} />Buku {nilai}</li>
            ))}
            </ul>
        </>
    )
}

export default Buku;