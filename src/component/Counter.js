import React from 'react'
import { connect } from 'react-redux'
// import { actionTypes } from '../redux/action';
import { actionTypes } from '../redux/order/action';

const Counter = (props) => {
    return(
        <>
            <button onClick={props.counterMinusFunction}>-</button>
            <button onClick={props.counterPlusFunction}>+</button>
            <br/>
            {props.order}
            <br/>
            <button onClick={props.counterMinusPrice500}>- 500 price</button>
            <button onClick={props.counterPlusPrice500}>+ 500 price</button>
            <br />
            {props.price}
            <p>Nama: {props.nama}</p>
            <div>
                <button onClick={props.changeNameFunction}>Change Name</button>
            </div>
        </>
    );
};

// const stateToProps = (globalState) => {
//     return {
//         order: globalState.order,
//         price: globalState.price,
//         isLogin: globalState.isLogin,
//         nama: globalState.nama
//     };
// };

const stateToProps = (globalState) => {
    return {
        order: globalState.order.order,
        price: globalState.order.price,
        isLogin: globalState.order.isLogin,
        nama: globalState.order.nama
    };
};

const dispatchToProps = (dispatch) => {
    return{
        counterPlusFunction: () => dispatch({type: "PLUS_ORDER"}),
        counterMinusFunction: () => dispatch({type: "MINUS_ORDER"}),
        counterMinusPrice500: () => dispatch({type: "MINUS_PRICE_500"}),
        counterPlusPrice500: () => dispatch({type: actionTypes.PLUS_PRICE_500}),
        changeNameFunction: () => dispatch({type: actionTypes.CHANGE_NAME, payload: 'Oke'})
    };
};

export default connect(stateToProps, dispatchToProps)(Counter);