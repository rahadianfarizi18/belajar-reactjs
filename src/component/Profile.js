import { Component } from 'react';
import { connect } from 'react-redux'
import About from './About';

class Profile extends Component {
    constructor(props) {
        super(props);
        this.state = {
            name: this.props.user.nama,
            isSubmitted: false
        }
        this.setName = this.setName.bind(this);
    }
  
    setName(props) {
        this.setState({
            name: this.props.name
        })
    }
  
    handleChange = (e) => {
        e.preventDefault();
        this.setState({
            name: e.target.value
        })
        console.log(this.state.name);
    }

    handleSubmit = (e) => {
        e.preventDefault();
        this.setState({
            name: document.getElementById('name').value,
            isSubmitted: true
        })
        console.log(this.state.name)
        
    }
  
    render() {
        if(this.state.isSubmitted || this.props.isLogin) {
            return (
                // <Redirect to={redirectLoc} />
                <About name={this.state.name}/>
            )
        }
        return(
            // <div className='row'>
                <form onSubmit={this.handleSubmit} onChange={this.handleChange} className='row m-3'>
                    <div className='col-2'>
                        <label for="name" className="col-form-label">Masukkan nama anda: </label>
                    </div>
                    <div className='col-4'>
                        <input type="text" className="form-control" id="name" />
                    </div>
                    <div className='col-2'>
                        <button type="submit">Submit</button>
                    </div>
                </form>
            // </div>
        )
    }
  }

// const stateToProps = (globalState) => ({
//     user: globalState.user,
//     isLogin: globalState.isLogin,
// })

const stateToProps = (globalState) => ({
    user: globalState.auth.user,
    isLogin: globalState.auth.isLogin,
})

export default connect(stateToProps, null)(Profile);