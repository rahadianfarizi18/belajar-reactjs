import { Component } from 'react';

class ToDoList extends Component {
    constructor(props) {
        super(props)
        this.state = {
            item : '',
            arrItem: []
        }
    }

    handleChange = (e) => {
        e.preventDefault();
        this.setState({
            item: e.target.value
        })
        console.log(this.state.item);
    }

    handleSubmit = (e) => {
        e.preventDefault();
        console.log(this.state.item);
        this.setState({
            arrItem: [...this.state.arrItem, this.state.item],
            item : ''
        })
    }

    render() {
        return (
            <div>
              <form onSubmit={this.handleSubmit}>
                    <label for="nama">To Do: </label>
                    <input type="text" value={this.state.item} onChange={this.handleChange} />
                    <button type="submit">Submit</button>
                </form>
                {/* {console.log(this.state.arrItem)} */}
                <PrintItems arrItem={this.state.arrItem} />
            </div>
        )
    }
}


function PrintItems(props) {
    return(
        <ul>
            {props.arrItem.map((item, index) => 
                <li key={index}>{item}</li>
            )}
        </ul>
    )
}



export default ToDoList;