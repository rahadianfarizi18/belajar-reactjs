// import logo from './logo.svg';
import foto from '../foto.jpeg';
// import './App.css';
import '../assets/styles/profile.css';
import logoGithub from '../github.png'
import logoGitlab from '../gitlab.svg'
import logoLinkedIn from '../linkedin.png'
import logoInstagram from '../instagram.png'
import { Component } from 'react';
class About extends Component {
    constructor(props) {
        super(props)
        this.state = {
            name: this.props.name
        }
        this.visitorName = this.visitorName.bind(this);
    }
    

    visitorName(props) {
        return(
            <>{this.props.name}</>
        )
    }

    render() {
        return(
            <>
                <div className='m-3'>
                    <div>
                        <h1 className='my-3'>Hello, {this.state.name}, This Is My Profile</h1>
                    </div>
                    <div class="row">
                        <div class="col-2 pr-5">
                            <div class="row mb-2">
                                <img class="mx-auto" src={foto} alt="" />
                            </div>
                            <div class="row">
                                <table class="logo-bar">
                                    <tr>
                                        <td><a href="https://www.linkedin.com/in/rahadian-farizi/" target="_blank" rel="noreferrer"><img src={logoLinkedIn} title="LinkedIn" alt="" class="logo"/></a></td>
                                        <td><a href="http://instagram.com/rahadianfarizi/" target="_blank" rel="noreferrer"><img src={logoInstagram} title="Instagram" alt="" class="logo"/></a></td>
                                        <td><a href="https://github.com/rahadianfarizi18" target="_blank" rel="noreferrer"><img src={logoGithub} title="github" alt="" class="logo"/></a></td>
                                        <td><a href="https://gitlab.com/rahadianfarizi18" target="_blank" rel="noreferrer"><img src={logoGitlab} title="gitlab" alt="" class="logo"/></a></td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <div class="col-8">
                            <table class="table table-sm table-borderless">
                                <tr>
                                    <td class="title-col">Nama</td>
                                    <td>:</td>
                                    <td>Rahadian Farizi</td>
                                </tr>
                                <tr>
                                    <td class="title-col">NIK</td>
                                    <td>:</td>
                                    <td>920225</td>
                                </tr>
                                <tr>
                                    <td class="title-col">Tempat, tanggal lahir</td>
                                    <td>:</td>
                                    <td>Bandung, 18 Desember 1992</td>
                                </tr>
                                <tr>
                                    <td class="title-col">Pendidikan</td>
                                    <td>:</td>
                                    <td>S1 Teknik Telekomunikasi ITB</td>
                                </tr>
                                <tr>
                                    <td class="title-col">Hobi</td>
                                    <td>:</td>
                                    <td>- Running</td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td>- Console Gaming</td>
                                </tr>
                                <tr>
                                    <td class="title-col">Pengalaman kerja</td>
                                    <td>:</td>
                                    <td>- Officer 3 Support & Secretary DSS (2016 - 2017)</td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td>- Officer 3 Business Planning & Performance DSS (2017 - 2018)</td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td>- Officer 2 Business Planning & Performance DSS (2018 - 2021)</td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td>- Officer 1 Business Planning & Performance DSS (2021 - now)</td>
                                </tr>
                            </table>
                            <figure>
                                <p class="title-col">Favorite Quote</p>
                                <blockquote class="blockquote">
                                    <p>"Drangan trakioiot"</p>
                                </blockquote>
                                <figcaption class="blockquote-footer">
                                    <cite>Isyana Sarasvati</cite>
                                </figcaption>
                            </figure>
                            <div>
                                {/* <Toggle /> */}
                            </div>
                            {/* <ToDoList /> */}
                        </div>
                    </div>
                </div>
            </>
        )
    }
}

export default About;