import { Form, Button, Alert } from "react-bootstrap";
import { Link } from "react-router-dom";
// import "../assets/dist/css/bootstrap.min.css";
import "../assets/styles/login.scss"
import logo from "../assets/logo/logo-RF.png"
import { useState, useEffect, useCallback } from "react";
// import { registerUserAPI, writeUserData } from "../redux/action";
import { registerUserAPI, writeUserData } from "../redux/auth/action";
import { connect } from "react-redux";
import { useHistory } from "react-router";
import Particles from "particlesjs";

const Register = (props) => {
    const [name, setName] = useState('');
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [isRegister, setIsRegister] = useState(false);
    const history = useHistory();

    const handleChangeName = (e) => {
        e.preventDefault();
        setName(e.target.value)
    }
    
    const handleChangeEmail = (e) => {
        e.preventDefault();
        setEmail(e.target.value)
    }
    
    const handleChangePassword = (e) => {
        e.preventDefault();
        setPassword(e.target.value)
    }
    
    const handleSubmit = async (e) => {
        e.preventDefault()
        const res = await props.registerAPI({email, password}).catch(err=>err)
        if(res) {
            console.log(name)
            console.log(email)
            console.log(res.uid)
            await props.writeUserAPI({name, email, uid:res.uid})
            setName('');
            setEmail('');
            setPassword('');
            setIsRegister(true);
        }
    }

    const loadParticles = () => {
        Particles.init ({ 
            selector: '.background',
            color: '#8790a3',
            connectParticles: true, 
            speed: 0.4,
        });
    }

    // useCallback(
    //     () => {
    //         loadParticles()
    //     },[]
    // )
    
    useEffect(() => {
        loadParticles()
    },[])

    return(
        <>
        {isRegister ? <Alert variant='success' className='alert'>Your account has been registered! <Link to='/login'>Click here to login</Link></Alert> : <></> }
        <div className='form-signin text-center'>
            {/* <div className='d-flex'> */}
                <Form type='submit' className='shadow p-4 bg-body rounded'>
                    <img class="mb-2 mx-auto" src={logo} alt="" width="72" height="72" />
                    <h1 className='h3 mt-2 fw-normal'>Register</h1>
                    <Form.Control type='text' id='name' placeholder='Name' onChange={handleChangeName} value={name}/>
                    <Form.Control type='email' id='email' placeholder='Email' className='mt-2' onChange={handleChangeEmail} value={email}/>
                    <Form.Control type='password' id='password' placeholder='Password' className='mt-2' onChange={handleChangePassword} value={password}/>
                    {/* <Form.Text type='password' id='confirmPassword' placeholder='Confirm Password' className='mt-2'/> */}
                    <Button color='primary' type='submit' className='w-100 mt-2 btn-lg' onClick={handleSubmit}>
                        {props.isLoading ? 
                            (
                                <>
                                <span className='spinner-border spinner-border-sm'></span>
                                {'  '}Loading...
                                </>
                            ) :
                            "Submit"}
                    </Button>
                    <p className='mt-2'>Already have account? <Link to='/login'>Login</Link></p>
                </Form>
            {/* </div> */}
        </div>
        <canvas className='background'/>
        </>
    )
}

// const propsToState = (globalState) => ({
//     isLoading: globalState.isLoading,
//     isRegister: globalState.isRegister,
// })

const propsToState = (globalState) => ({
    isLogin: globalState.auth.isLogin,
    isLoading: globalState.auth.isLoading,
    isRegister: globalState.auth.isRegister,
})

const dispatchToProps = (dispatch) => ({
    registerAPI: (data) => dispatch(registerUserAPI(data)),
    writeUserAPI: (data) => dispatch(writeUserData(data)),
})

export default connect(propsToState, dispatchToProps)(Register);