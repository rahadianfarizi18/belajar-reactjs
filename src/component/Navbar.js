import { Link, NavLink } from 'react-router-dom'
import { connect } from 'react-redux'
import logo from '../assets/logo/logo-RF.png'
import "../assets/styles/navbar.css"

const styling = {
    brand: {
        'padding-left': '0px',
        // position: 'absolute',
        // 'padding-left': '10px'
    },
    items: {
        'padding-left': '0px',
    },
    loginLogout: {
        'padding-right': '0px',
        'width' : '60px'
    }
}

const Navbar = (props) => {

    const navbarLogin = () => (
        <>
            <ul className="navbar-nav me-auto mb-2 mb-lg-0 ms-auto navbar-style" style={styling.items}>
                <li className="nav-item">
                    <NavLink className='nav-link link' activeClassName="link-active nav-link" aria-current="page" to="/profile">
                        Profile
                    </NavLink>
                </li>
                <li className="nav-item">
                    <NavLink className='nav-link link' activeClassName="link-active nav-link" to="/bus-management">
                        Bus Management
                    </NavLink>
                </li>
                <li className="nav-item">
                    <NavLink className='nav-link link' activeClassName="link-active nav-link" to="/hooks">
                        Hooks
                    </NavLink>
                </li>
                <li className="nav-item">
                    <NavLink className='nav-link link' activeClassName="link-active nav-link" to="/contributor">
                        Contributor
                    </NavLink>
                </li>
                <li className="nav-item">
                    <NavLink className='nav-link link' activeClassName="link-active nav-link" to="/pokemon">
                        Pokemon
                    </NavLink>
                </li>
                <li className="nav-item">
                    <NavLink className='nav-link link' activeClassName="link-active nav-link" to="/buku">
                        Buku
                    </NavLink>
                </li>
                <li className="nav-item">
                    <NavLink className='nav-link link' activeClassName="link-active nav-link" to="/counter">
                        Counter
                    </NavLink>
                </li>
            </ul>
            <Link className="text-light text-decoration-none" to="/logout" style={styling.loginLogout}>Logout</Link>
        </>
    )

    const navbarNotLogin = () => (
        <>
            <ul className="navbar-nav me-auto mb-2 mb-lg-0 ms-auto" style={styling.items}>
                <li className="nav-item">
                    <Link className="nav-link" aria-current="page" to="/profile">Profile</Link>
                </li>
            </ul>
            <Link className="text-light text-decoration-none" to="/login" style={styling.loginLogout}>Sign in</Link>
        </>
    )

    return (
            <nav className="navbar navbar-dark navbar-expand-lg bg-dark mb-4" style={{position: 'relative', zIndex:'1' }}>
                <div className="container-fluid" > 
                    <Link className="navbar-brand" to="/" style={styling.brand}>
                        <img alt='home' src={logo} width='24' height='24' className="mt-1 float-start align-text-top me-2"/>
                        <div style={{width: '36px', height:'0px'}}>RF Site</div>
                    </Link>
                    <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"></span>
                    </button>
                    <div className="collapse navbar-collapse" id="navbarSupportedContent">                    
                        {props.isLogin ? navbarLogin() : navbarNotLogin() }
                    </div>
                </div>
            </nav>
    )
}

// const stateToProps = (globalState) => {
//     return {
//         isLogin: globalState.isLogin,
//     };
// };

const stateToProps = (globalState) => {
    return {
        isLogin: globalState.auth.isLogin,
    };
};

export default connect(stateToProps, null)(Navbar);