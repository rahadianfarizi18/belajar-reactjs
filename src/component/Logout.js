import { Redirect } from "react-router-dom";
// import { actionTypes } from "../redux/action";
import { actionTypes } from "../redux/auth/action";
import { connect } from "react-redux";

const Logout = (props) => {
    props.logoutFunction();
    return(
        <Redirect to='/'/>
    )
}

// const stateToProps = (globalState) => {
//     return {
//         isLogin: globalState.isLogin,
//     };
// };

const stateToProps = (globalState) => {
    return {
        isLogin: globalState.auth.isLogin,
    };
};

const dispatchToProps = (dispatch) => {
    return{
        loginFunction: () => dispatch({type: actionTypes.LOGIN}),
        logoutFunction: () => dispatch({type: actionTypes.LOGOUT}),
    };
};

export default connect(stateToProps, dispatchToProps)(Logout);