import React, { useState, useEffect, useCallback } from 'react';
import { BrowserRouter as Router, Route, Switch, Link } from 'react-router-dom';
import NewBus from './component/NewBus';
import ExistingBus from './component/ExistingBus';
import ChangeCapacity from './component/ChangeCapacity';
import ShowBus from './component/ShowBus';
import Button from 'react-bootstrap/Button';
import { connect } from 'react-redux';
import { actionTypes, writeBusAPI } from '../../redux/bus/action';

// const initialStateArrBus = [];

const BusManagement = (props) => {
    const [ arrBus, setArrBus ] = useState(props.arrBus);
    const path = '/bus-management';
    const [ isArrBusFilled, setIsArrBusFilled ] = useState(false)
    const { uid, changeBusFunction, writeBusDataFunc } = props
    
    console.log(arrBus)

    const handleBusChange = (newArrBus) => {
        setArrBus(newArrBus);
    }

    // useCallback(
    //     () => {
    //         const uid = props.uid
    //         props.writeBusDataFunc({uid, arrBus})
    //     },
    //     [arrBus],
    // )

    useEffect(() => {
        // const uid = props.uid
        for (let bus of arrBus) {
            if(bus.listPenumpang === undefined) {
                bus.listPenumpang = []
            }
        }
        setIsArrBusFilled(Boolean(arrBus.length));
        changeBusFunction(arrBus);
        writeBusDataFunc({uid, arrBus})
    }, [arrBus, changeBusFunction, uid, writeBusDataFunc])

    return(
        <Router basename={`${path}`}>
            <p className="fs-1 mb-3 fw-bolder">Bus Management</p>
            <div id="start-button" className="mb-3">
                <div className="row">
                    <label className="fs-6 mb-2 fw-light fst-italic">Please select:</label>
                </div>
                <div className="row">
                    <div className="col-auto">
                        <Link to='/new-bus'>
                            <Button variant='outline-primary'>Tambah Bus Baru</Button>
                        </Link>
                    </div>
                    <div className="col-auto">
                        <Link hidden={!isArrBusFilled} to='/change-capacity'>
                            <Button variant='outline-success' id="change-capacity">Change Bus Capacity</Button>
                        </Link>
                    </div>
                    <div className="col-auto">
                        <Link hidden={!isArrBusFilled} to='/existing-bus'>
                            <Button variant='outline-dark' id="existing-bus">Tambah/Turun Penumpang</Button>
                        </Link>
                    </div>
                </div>
            </div>
            <Switch>
                {/* <Route exact path='/'> */}
                {/* </Route> */}
                <Route exact path='/new-bus' >
                    <NewBus 
                        arrBus={arrBus}
                        onBusAdd={(newArrBus) => handleBusChange(newArrBus)}
                    />
                </Route>
                <Route path="/existing-bus">
                    <ExistingBus
                        arrBus={arrBus}
                        onBusChange = {(newArrBus) => handleBusChange(newArrBus)}
                    />
                </Route>
                <Route path='/change-capacity' exact>
                    <ChangeCapacity 
                        arrBus={arrBus}
                        onChangeCapacity = {(newArrBus) => handleBusChange(newArrBus)}
                    />
                </Route>
            </Switch>
            <ShowBus arrBus={arrBus}/>
        </Router>
    )
}

const dispatchToProps = (dispatch) => {
    return {
        changeBusFunction: (arrBus) => dispatch({type: actionTypes.CHANGE_ARR_BUS, value: arrBus }),
        writeBusDataFunc: (uid, arrBus) => dispatch(writeBusAPI(uid, arrBus)),
    }
}

const stateToProps = (globalState) => {
    return {
        uid: globalState.auth.user.uid,
        arrBus: globalState.bus.arrBus,
    }
}

export default connect(stateToProps, dispatchToProps)(BusManagement);