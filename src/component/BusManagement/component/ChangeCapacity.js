import { busFunctions } from "./busFunctions";
import { useState, useEffect } from "react";
import { Redirect } from "react-router-dom";
import { Col, Form, Button } from 'react-bootstrap';

const ChangeCapacity = (props) => {
    const [arrBus, setArrBus] = useState(props.arrBus);
    const [busCapacity, setBusCapacity] = useState();
    const [busName, setBusName] = useState(props.arrBus[0].name)
    const [isSubmitted, setIsSubmitted] = useState(false)

    const handleChangeBusName = (e) => {
        e.preventDefault();
        setBusName(e.target.value)
        console.log(busName)
    }
    
    const handleChangeBusCapacity = (e) => {
        e.preventDefault();
        setBusCapacity(Number(e.target.value))
        console.log(busCapacity)
    }

    const handleSubmit = (e) => {        
        e.preventDefault();
        let result;
        function findBus () {
            for (let key in arrBus) {
                if(arrBus[key].name === busName) {
                    result=key
                }
            }
            return result;
        }
        console.log('bus name => ', busName)
        console.log('find Bus => ',findBus())
        const changedBus = arrBus[findBus()]
        busFunctions.setCapacity(changedBus, busCapacity)
        let arrBusCopy = [...arrBus]
        arrBusCopy[findBus()] = changedBus
        setArrBus(arrBusCopy)
        setIsSubmitted(true)
    }

    useEffect(() => {
        props.onChangeCapacity(arrBus)
    }, [arrBus, props])

    if(isSubmitted) {
        return(
            <Redirect to='/' />
        )
    } else {
        return(
            <>
                <Form type='submit' onSubmit={handleSubmit} className='m-4 container-sm shadow p-4 bg-light'>
                    <p className='h3 text-primary fw-bold p-2 rounded mb-4'>Form Change Bus Capacity</p>
                    <Form.Group row className='mb-3 d-flex justify-content-sm-start'>
                        <Form.Label for='select-bus' sm={2} className='mx-1 d-flex w-25'>Select Bus:</Form.Label>
                        <Form.Select type='select' name='select' id='select-bus' className='mx-1 flex-shrink-1 w-auto shadow-sm' onChange={ handleChangeBusName }>
                            <option disabled>Select bus</option>
                            {arrBus.map((bus,i) => (
                                <option value={bus.name}>{bus.name}</option>
                            ))}
                        </Form.Select>
                    </Form.Group>
                    <Form.Group row id="form-capacity" className="mb-3 d-flex justify-content-start">
                        <Form.Label for="capacity" className="mx-1 d-flex w-25">Bus Capacity:</Form.Label>
                        <Form.Control sm={3} type="number" id="capacity" className="mx-1 w-50 shadow-sm w-auto" value={busCapacity} onChange={handleChangeBusCapacity}/>
                    </Form.Group>
                    <div>
                        <Button variant='primary' type='submit'>Submit</Button>
                    </div>
                </Form>
            </>
        )
    }
}

export default ChangeCapacity;