export const busFunctions = {
    
    setCapacity: (bus, cap) => {
        if (typeof (cap) !== 'number') {
            alert('Capacity harus angka!');
        } else if (cap <= 0) {
            alert('Capacity harus lebih dari 0!');
        } else {
            bus.capacity = cap;
        }
    },
    
    tambahPenumpang: (bus, penumpang) => {
        if (typeof (penumpang) !== 'string') {
            console.log('Nama penumpang harus string!');
        } else {
            const jumlahKursiTerisi = bus.listPenumpang.filter((pen) => Boolean(pen)).length;
            if (jumlahKursiTerisi >= bus.capacity) {
                alert('Bus penuh!');
                return;
            } else {
                for (let idxPenumpang in bus.listPenumpang) {
                    if (bus.listPenumpang[idxPenumpang] === penumpang) {
                        alert(`${penumpang} sudah ada di dalam bus!`);
                        return;
                    } else if (!bus.listPenumpang[idxPenumpang]) {
                        bus.listPenumpang[idxPenumpang] = penumpang;
                        return;
                    }
                }
                bus.listPenumpang.push(penumpang);
            }
        }
    },
    
    turunPenumpang: (bus, penumpang) => {
        let index = bus.listPenumpang.indexOf(penumpang);
        if (index !== -1) {
            bus.listPenumpang[index] = null;
        } else {
            console.log(`${penumpang} tidak ada di dalam bus!`)
            alert(`${penumpang} tidak ada di dalam bus!`)
        }
    }

}
