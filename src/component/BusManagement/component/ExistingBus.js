import {busFunctions} from './busFunctions';
import { useState, useEffect } from "react";
import { Redirect, useLocation } from "react-router-dom";
import { Col, Form, Button } from 'react-bootstrap';

const ExistingBus = (props) => {
    const location = useLocation()
    const [arrBus, setArrBus] = useState(props.arrBus);
    const [busName, setBusName] = useState(location.state ? location.state.busName : props.arrBus[0].name)
    const [isSubmitted, setIsSubmitted] = useState(false)
    const [namaPenumpang, setNamaPenumpang] = useState('')

    const handleChangeBusName = (e) => {
        e.preventDefault();
        setBusName(e.target.value)
        console.log(busName)
    }

    const handleTambahPenumpang = (e) => {
        e.preventDefault();
        let result;
        function findBus () {
            for (let key in arrBus) {
                if(arrBus[key].name === busName) {
                    result = key
                }
            }
            return result;
        }
        const busToChange = arrBus[findBus()]
        busFunctions.tambahPenumpang(busToChange, namaPenumpang)
        let arrBusCopy = [...arrBus]
        arrBusCopy[findBus()] = busToChange
        setArrBus(arrBusCopy)
        setIsSubmitted(true)
    }

    const handleTurunPenumpang = (e) => {
        e.preventDefault();
        let result;
        function findBus () {
            for (let key in arrBus) {
                if(arrBus[key].name === busName) {
                    result = key
                }
            }
            return result;
        }
        const busToChange = arrBus[findBus()]
        busFunctions.turunPenumpang(busToChange, namaPenumpang)
        let arrBusCopy = [...arrBus]
        arrBusCopy[findBus()] = busToChange
        setArrBus(arrBusCopy)
        setIsSubmitted(true)
    }

    const handleChangeNamaPenumpang = (e) => {
        e.preventDefault();
        setNamaPenumpang(e.target.value)
    }

    useEffect(() => {
        props.onBusChange(arrBus)
    }, [arrBus, props])

    if(isSubmitted) {
        return(
            <Redirect to='/' />
        )
    } else {
        return(
            <>
                <Form type='submit' className='m-4 container-sm shadow p-4 bg-light'>
                    <p className='h3 text-primary fw-bold p-2 rounded mb-4'>Form Naik/Turun Penumpang</p>
                    <Form.Group className='mb-3 d-flex justify-content-sm-start'>
                        <Form.Label for='select-bus' className='mx-1 d-flex w-25'>Select Bus:</Form.Label>
                        <Form.Select className='mx-1 flex-shrink-1 w-auto shadow-sm' name='select' id='select-bus' value={busName} onChange={ handleChangeBusName }>
                            <option disabled>Select bus</option>
                            {arrBus.map((bus,i) => (
                                <option value={bus.name}>{bus.name}</option>
                            ))}
                        </Form.Select>
                    </Form.Group>
                    <Form.Group className='mb-3 d-flex justify-content-start'>
                        <Form.Label for="name" className='mx-1 d-flex w-25'>Nama penumpang:</Form.Label>
                        <Form.Control type='text' className='mx-1 flex-shrink-1 w-auto shadow-sm' id="capacity" value={namaPenumpang} onChange={handleChangeNamaPenumpang}/>
                    </Form.Group>
                    <div className='mt-4'>
                        <Button variant='primary' onClick={handleTambahPenumpang}>Tambah Penumpang</Button> {' '}
                        <Button variant='danger' onClick={handleTurunPenumpang}>Turun Penumpang</Button>
                    </div>
                </Form>
            </>
        )
    }
}

export default ExistingBus;