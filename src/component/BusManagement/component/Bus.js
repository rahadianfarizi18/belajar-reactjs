class Bus {
    constructor(name, capacity) { //class constructor
        this._name = name;
        this._capacity = capacity;
        this._listPenumpang = [];
    }
    get name() {
        return this._name;
    }
    get capacity() {
        return this._capacity;
    }
    get listPenumpang() {
        return this._listPenumpang;
    }
    set capacity(cap) {
        if (typeof (cap) !== 'number') {
            alert('Capacity harus angka!');
        } else if (cap <= 0) {
            alert('Capacity harus lebih dari 0!');
        } else {
            this._capacity = cap;
        }
    }

    tambahPenumpang(penumpang) {
        if (typeof (penumpang) !== 'string') {
            console.log('Nama penumpang harus string!');
        } else {
            const jumlahKursiTerisi = this.listPenumpang.filter((pen) => Boolean(pen)).length;
            if (jumlahKursiTerisi >= this.capacity) {
                alert('Bus penuh!');
                return;
            } else {
                for (let idxPenumpang in this.listPenumpang) {
                    if (this.listPenumpang[idxPenumpang] === penumpang) {
                        alert(`${penumpang} sudah ada di dalam bus!`);
                        return;
                    } else if (!this.listPenumpang[idxPenumpang]) {
                        this.listPenumpang[idxPenumpang] = penumpang;
                        return;
                    }
                }
                this.listPenumpang.push(penumpang);
            }
        }
    }

    turunPenumpang(penumpang) {
        let index = this.listPenumpang.indexOf(penumpang);
        if (index !== -1) {
            this.listPenumpang[index] = null;
        } else {
            console.log(`${penumpang} tidak ada di dalam bus!`)
            alert(`${penumpang} tidak ada di dalam bus!`)
        }
    }
}

export default Bus;