// import Bus from './Bus';
// import BusFunction from './Bus';
import { useState, useEffect} from 'react';
import { Redirect } from 'react-router-dom';
import { Form, Button, Col } from 'react-bootstrap';
import { connect } from 'react-redux';
import { writeBusAPI } from '../../../redux/bus/action';
import '../../../assets/styles/newBus.scss'

const NewBus = (props) => {
    const [arrBus, setArrBus] = useState(props.arrBus)
    const [busName, setBusName] = useState('')
    const [busCapacity, setBusCapacity] = useState(0)
    const [isSubmitted, setIsSubmitted] = useState(false)

    const handleChangeBusName = (e) => {
        e.preventDefault();
        setBusName(e.target.value)
    }

    const handleChangeBusCapacity = (e) => {
        e.preventDefault();
        setBusCapacity(Number(e.target.value))
    }

    const handleSave = async (e) => {
        e.preventDefault();
        const uid = props.uid
        console.log(uid)
        if(!busName) {
            alert('Nama bus tidak boleh kosong!')
        } else if (busCapacity <=0 ) {
            alert('Bus capacity harus lebih dari 0!')
        } else {
            const busToAdd = {
                name: busName,
                capacity: busCapacity,
                listPenumpang: [],
            }
            setArrBus(
                [...arrBus,busToAdd]
            )
            console.log(arrBus)
            alert('Bus sudah disimpan.')
            setIsSubmitted(true)
            }
        }
    

    useEffect(()=>{
        props.onBusAdd(arrBus)
    },[arrBus, props])


    if(isSubmitted) {
        return(
            // history.push('/bus-management')
            // <Router>
                <Redirect to='/'/>
            // </Router>
        )
    } else {
        return( 
            <>    
                <form className='m-4 container-xs shadow p-4 bg-light'>
                    {/* <h4>Form New Bus</h4> */}
                    {/* <div className='d-flex flex-row'> */}
                    <p className='h3 text-primary fw-bold p-2 rounded mb-4'>Form New Bus</p>
                    {/* </div> */}
                    <div className='container'>
                        <div className="mb-3 row form-container" id="form-bus">
                            <Col sm={2}>
                                <Form.Label for="bus" className="col-sm">Nama Bus:</Form.Label>
                            </Col>
                            <Col sm={6}>
                                <Form.Control type="text" id="bus-name" className="mx-1 shadow-sm col" value={busName} onChange={handleChangeBusName}/>
                            </Col>
                        </div>
                        <div id="form-capacity" className="mb-3 row form-container">
                            <Col sm={2}>
                                <Form.Label for="capacity" className="col" >Bus Capacity:</Form.Label>
                            </Col>
                            <Col sm={6}>
                                <Form.Control type="number" id="capacity" className="mx-1 shadow-sm col" onChange={handleChangeBusCapacity}/>
                            </Col>
                        </div>
                        <Button variant='primary' onClick={handleSave} className='shadow-sm'>Submit</Button>
                        {/* <button type="submit">Submit</button> */}
                    </div>
                </form>
                {/* <Button onClick={logArrBus}>Log Arr Bus</Button> */}
            </>
        )
    }
}

const stateToProps = (globalState) => ({
    uid: globalState.auth.user.uid,
})

const dispatchToProps = (dispatch) => ({
    writeBusDataFunc: (uid, arrBus) => dispatch(writeBusAPI(uid, arrBus))
})

export default connect(stateToProps, dispatchToProps)(NewBus);