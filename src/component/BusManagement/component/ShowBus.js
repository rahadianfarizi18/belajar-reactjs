import { Card, Row, Col, Container } from "react-bootstrap"
import imgBus from '../../../assets/images/bus.jpeg'
import { Link, useLocation } from "react-router-dom"

const ShowBus = (props) => {
    const arrBus = props.arrBus
    console.log(arrBus)

    return(
        <Container>
            <p className='h3' hidden={Boolean(!arrBus.length)}>Daftar Bus:</p>
            <p className='text-muted mb-3' hidden={Boolean(!props.arrBus.length)}>Klik bus untuk naik/turun penumpang</p>
            <Container className='shadow p-4 bg-dark bg-gradient rounded' hidden={Boolean(!props.arrBus.length)}>
                <Row>
                {arrBus.map((data,i) => (
                    <Col sm={3} className=''>
                        <Link className='text-decoration-none text-dark' to={{pathname: '/existing-bus', state:{ busName: data.name }}}>
                            <Card className='shadow-sm'>
                                <Card.Img top width='100%' src={imgBus}/>
                                <Card.Body>
                                    <Card.Title tag='h5'>Bus {data.name}</Card.Title>
                                    <Card.Subtitle tag='h6'>Kapasitas: {data.capacity}</Card.Subtitle>
                                    <Card.Text>
                                        {data.listPenumpang ? 
                                            <>
                                                <span>Penumpang:</span>
                                                <ol>
                                                    {data.listPenumpang.map((penumpang,i) => (
                                                        <li key={i}>{penumpang}</li>
                                                    ))}
                                                </ol>
                                            </>
                                            :
                                            <></>
                                        }
                                    </Card.Text>
                                </Card.Body>
                            </Card>
                        </Link>
                    </Col>
                ))}
                </Row>
            </Container>
        </Container>
    )
    
}

export default ShowBus;