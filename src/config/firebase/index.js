import firebase from 'firebase/app';
import 'firebase/auth';
import 'firebase/database';

const firebaseConfig = {
    apiKey: "AIzaSyAouWZBElSgqZ-c3wKxTR31qQaxdODXClc",
    authDomain: "belajar-reactjs-5dc1d.firebaseapp.com",
    projectId: "belajar-reactjs-5dc1d",
    storageBucket: "belajar-reactjs-5dc1d.appspot.com",
    messagingSenderId: "444003676114",
    appId: "1:444003676114:web:8154313b827c6e56f3220b",
    measurementId: "G-CT5XGX17HZ",
    databaseURL: "https://belajar-reactjs-5dc1d-default-rtdb.asia-southeast1.firebasedatabase.app/",
  };
  // Initialize Firebase
  firebase.initializeApp(firebaseConfig);
  // firebase.analytics();

export const database = firebase.database();

export default firebase;